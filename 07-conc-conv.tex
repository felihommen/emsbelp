% !TEX root = twibop2.tex
\chapter*{A Concluding Conversation}
\addcontentsline{toc}{chapter}{A Concluding Conversation}
\epigraph{It is only when we finish writing that we find what we should have begun from.}{Blaise Pascal}

\begin{dialogue}

%{\parindent=0pt
\athr ``This book on the world of probabilities has come to an
end. I hope that it gave some food for thought.''


\rdr ``I have to admit that some points do not fit in with my own
views. For instance, it is hard for me to see how randomness can be
used to solve problems. I mean the perceptron, the Monte Carlo
method, and the principle of homeostat. These are very much like
`miracles'.''

\athr ``In the meantime, they are just as `miraculous' as the
random number table.''

\rdr ``I do not understand.''

\athr ``Each new digit in the table is independent of its
predecessors. In spite of that, the table as a whole has stability. The
digits appear independently from each other, but the frequency in
which any digit appears is determinate.''

``Besides, it is useless to try and write down a set of random digits
`by hand'. For instance, you might write 8, 2, 3, 2, 4, 5, 8, 7 
\ldots{}\, And naturally, you see that perhaps you should write a 1 or a 6 because the digits are not in the sequence. And against your will, you correct your actions as a result of your preceding ones. The result is that you won't have a table of truly random numbers.''

``It is essential to see that the occurrence of each random event is in
no way related to the preceding ones. Therefore, the stability observed
in the picture of a large number of random events seems to be
`miraculous'. In the long run, the `miracle' is responsible for the
properties of the perceptron or the Monte Carlo method.''

\rdr ``I can agree that the `root of the evil' hides, in the long run,
in a random number table. How can you explain the puzzling
properties of this table?''

\athr ``The explanation is in the word `symmetry'.''

\rdr ``Please explain.''

\athr ``Having found a digit to add to your table, you take care to
provide \redem{symmetry} with respect to the occurrence of all the other digits.
In other words, any digits from 0 to 9 should have the \redem{same chance} of
appearing.''

\rdr ``Suppose I have a bag and draw out balls labelled
with different digits. What kind of symmetry do you mean
here?''


\athr ``For instance, the symmetry with respect to the exchange of
the balls. Imagine that all the balls suddenly change places. If the
symmetry exists, you will not notice the exchange. But this is not all.
Once you return the balls to the bag and mix them, you restore the
initial situation and take care to make the system symmetrical with
respect to each act in which a ball is drawn. As you can see, the
explanation is deep enough. Symmetry and asymmetry are related to
the most fundamental notions. These notions underlie the scientific
picture of the universe.''


\rdr ``I have read your book \redem{This Amazingly Symmetrical World}.
I was really amazed how far symmetry penetrates into every
phenomenon occurring in this world. Now I see that the same can be
said about randomness.''

\athr ``Thank you. You refer to my book \redem{This Amazingly
Symmetrical World}, in which I attempted to set forth the notion of
\redem{symmetry} and show how the concepts of symmetry and asymmetry
underlie our physical picture of the world.''

``In fact, the point of that book was not just symmetry but the
\redem{dialectical unity of symmetry and asymmetry}. Here I was not just
considering randomness but the \redem{dialectical unity of necessity
and randomness}, which is, by the way, expressed in terms of probability.''

\rdr ``Judging from the remarks above, there seems to be
a relation between necessity-randomness and symmetry-asymmetry.''

\athr ``Yes, and a very profound one. The principles of
symmetry-asymmetry control both the laws of Nature and the laws of
human creativity. And the role of probabilistic principles is no less
fundamental.''

\rdr ``I'd like to discuss the relation between symmetry and
probability in more detail.''

\athr ``The classical definition of probability is underlain by the
idea of equally possible outcomes. In turn, \redem{equally possible outcomes always have a certain symmetry}. We dealt with equally possible
outcomes when we discussed throwing a die or tossing a coin. Recall
the definition of the statistical weight of a macro-state in terms of the
number of equally possible micro=states (Chapter 4), and recall our
discussion of equally possible alternatives while considering Mendel's
laws (Chapter 6). In each case, the probability of an event was defined as being proportional to the number of equally possible (I can now say, symmetrical) outcomes, in each of which the given event is realized. In other words, the probability of an event is the sum of the probabilities of the respective equally possible outcomes.''

\rdr ``I begin to think that the very rule of the \redem{summation of
probabilities} is based on a certain symmetry.''

\athr ``An interesting idea.''

\rdr ``Given we are looking for the probability that one of two
events will occur, it is irrelevant which one does because either of
them brings about a result. The symmetry here is related to the
independence with which the result is obtained with respect to the
substitution of one event for the other.''

\athr ``We can go further. Suppose there is a deeper symmetry
related to the \redem{indistinguishability} between the first and the second
event (similar situations were discussed in Chapter 5). The rule of the
\redem{summation of probabilities} is replaced in this case by the rule of the
\redem{summation of the probability amplitudes}.''

\rdr ``True, I can clearly see here the relation between symmetry
and probability.''

\athr ``This relation can be represented even more clearly if we
use the notion of \redem{information}. Of course, you remember that
information is underlain by probability in principle (see Chapter~3).
Now the relation between information and symmetry is as follows:
\redem{less information corresponds to a more symmetrical state}.''

\rdr ``Then it is possible to believe that an increase in the
symmetry of a state should result in a rise in its entropy.''

\athr ``Exactly. Have a look at \figr{entropy-disorder}. The state with the
greatest statistical weight, and therefore, with the greatest entropy is
the state corresponding to the uniform distribution of molecules in
both halves of the vessel. Evidently, this is the most symmetrical
arrangement (there is a mirror symmetry with respect to the plane
separating the vessel in two).''

\rdr ``That is something here to think over. It means that human
creativity reduces symmetry. However, symmetry is widely used in art.
Is this not a contradiction?''

\athr ``No. We use symmetry-asymmetry rather than only
symmetry in art. We have already discussed it elsewhere, in my book
on symmetry. Of course, these problems require special consideration.
We can only touch on the problems here and not go into any detail.''

``I emphasized in my book on symmetry that symmetry operates to
\redem{limit the number of possible variants of structure or variants of behaviour}. Obviously, necessity operates in the same direction. On the other hand, asymmetry operates to increase the number of possible variants. Chance acts in the same direction. I have repeatedly drawn your
attention to the fact that chance \redem{creates new possibilities} and gives rise to new alternatives.''

\rdr ``This means that we can speak of the `composition of forces'
as follows. There are symmetry and necessity on the one side, and
asymmetry and chance are on the other side.''

\athr ``Yes, this `composition of forces' is correct. Please recall the
parable about the `Buridan's ass'. I started with it my first
conversation `between the author and the reader' in \redem{This Amazingly
Symmetrical World}.''

\rdr ``I know this parable. The legend has it that a philosopher
named Buridan left his ass between two heaps of food. The ass
starved to death because he could not decide which heap to start
with.''

\athr ``The parable was an illustration of mirror symmetry. There
were two identical heaps of food and the ass at the same distance
between them. The ass was unable to make his choice.''

\rdr ``As I see it, the ass starved to death because of symmetry.''

\athr ``As the parable has it, he did. In reality, however, the ass
lived in the `symmetrical world built on probability' rather than in the
`symmetrical world' without any randomness. Any chance occurrence
(a fly could bother the ass, he could jerk or move a little) might easily
\redem{destroy the symmetry}: one of the heaps could become a bit closer, and the problem of choice is `null and void'. As physicists say,
a \redem{spontaneous violation of symmetry} could easily occur.''

\rdr ``Is it possible to conclude that symmetry is harmful while
chance is beneficial?''

\athr ``I'm sure you realise that such a question is too far reaching.
We have seen that symmetry decreases the number of versions of
behaviour and reduces a number of alternatives. It is logical to admit
that this reduction may lead to a hopeless situation, to a blind alley.
And then chance becomes essential. On the other hand; too many
chances, an abundance of alternatives and disorder may also be harmful.
And then, order comes to rescue, i.e. symmetry and necessity.''

\rdr ``The danger of randomness is understandable. But what
might be the danger of symmetry? If of course we exclude the
situation the `Buridan's ass' was in.''

\athr ``Firstly, the `Buridan's ass' was not an illustration from the
life of animals but rather the presentation of a problem. Secondly, it is
quite easy to give a practical example of the \redem{danger of symmetry}. Designers of bridges, towers, and skyscrapers know that they must not be too symmetrical because of the danger of resonance oscillation, which can destroy a construction. There are well-known accidents when bridges have been destroyed due to resonance caused, for
example, by a company of marching soldiers, rhythmic bursts of wind,
or other seemingly inoffensive causes. Therefore, when large
constructions are built, the symmetry is always violated in some way
by randomly placed asymmetric beams, panels, etc.''

\rdr ``True, symmetry may be dangerous. As far as I understand,
it is quite easy to destroy symmetry, be it a fly bothering an animal or
an extra beam in a construction.''

\athr ``Your attention has been drawn to an essential point. The
\redem{instability of symmetry} makes it easily upset and, in particular, allows for the possibility of spontaneous violation.''

\rdr ``Symmetry is unstable. This is something new to me.''

\athr ``The investigation of unstable symmetry has not been going long, only a decade. It has led to the appearance of a new scientific
discipline called \redem{catastrophe theory}. This theory studies the
relationship between symmetry and chance from the point of view of
the development of various processes and phenomena.''

\rdr ``The very name of the theory is somewhat dismal.''

\athr ``The catastrophes considered in the theory occur on
different levels. Suppose a particle causes a violent process in
a Geiger-M\"uller counter. The result is that the particle is registered.
The process is a catastrophe on the scale of the microcosm. An
enormous bridge or a jet plane may be suddenly brought down due to
resonance oscillations. This is a catastrophe on our common scale.
Catastrophes occur in a diversity of situations: sudden crystallization
in a supercooled liquid, a landslide, the start of laser generation, etc.
In each case, the system has an unstable symmetry, which may be
upset by a random factor. These random factors may be very slight in
influence, but they destroy the symmetry and therefore \redem{trigger violent processes in an unstable system}, and these processes are called
catastrophes.''

\rdr ``Catastrophe theory appears to show up the deep relationship
between symmetry-asymmetry and necessity-randomness
quite clearly.''

\athr ``I quite agree with you. However, it is a theme for another
book.''
\end{dialogue}

%%% Local Variables:
%%% mode: latex
%%% TeX-engine: xetex
%%% TeX-master: "twibop2"
%%% End:
