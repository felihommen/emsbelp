% !TEX root = twibop2.tex


\chapter*{Preface}
\addcontentsline{toc}{chapter}{Preface}
\markboth{Preface}{Preface}
\label{ch-preface}

\epigraph{\ldots In nature, where chance also seems to reign, we have
  long ago demonstrated in each particular field the inherent
  necessity and regularity that asserts itself in this
  chance.}{F. Engels}



A vast concourse of events and phenomena occur in the world around
us. The events are interrelated: some are effects or outcomes of others
which are, in turn, the causes of still others. Gazing into this gigantic whirlpool of interrelated phenomena, we can come to two significant conclusions. One is that there are both completely determined (uniquely defined) outcomes and ambiguous outcomes. While the former can be precisely predicted, the latter can only be treated probabilistically. The second, no less essential conclusion is that ambiguous outcomes occur much more frequently than completely determined ones. Suppose you press a button and the lamp on your desk lights up. The second event (the lamp lights up) is the completely determined result of the first event (the button is pressed). Such an event is called a \redem{completely determined} one. Take another example: a die is tossed. Each face of the die has a different number of dots. The die falls and the face with four dots ends up at the top. The second event in this case (four dots face-up) is not the completely determined outcome of the first event (the die is tossed). The top face may have contained one, two, three, five, or six dots. The event of appearance of the number of dots on the top face after a die is tossed is an example of a \redem{random} event. These examples clearly indicate the
difference between random and completely determined events.


We encounter random events (and randomness of various kinds) very
often, much more frequently than is commonly thought. The choice of
the winning numbers in a lottery is random. The final score of a
football match is random. The number of sunny days at a given
geographical location varies randomly from year to year. A set of
random factors underlies the completion of any service activity:
delivery ambulance arrival, telephone connection, etc.

Maurice Glaymann and Tamas Varga have written an interesting book
called \redem{Les Probabilit\'es \`a l'\'ecole} (Probability in Games
and Entertainment), in which they make an interesting remark: 
\begin{quote}
``When facing a chance situation, small children think that it is possible to  \redem{predict} its outcome. When they are a bit older, the believe that  \redem{nothing can be postulated}. Little by little they discover that there are patterns hiding behind the seeming chaos of the random world, and these patterns can be used to get their bearings in
reality.''
\end{quote}
There are three distinct stages here: lack of understanding
of the random at first, then mere confusion, and finally a correct
viewpoint. Let us forget small children for a time and try to apply
this to ourselves. We shall have to recognize that frequently we stop
at the first stage in a simple-minded belief that any outcome can be
precisely predicted. The misconception that randomness is simply equal
to chaos, or the absence of causality, has lasted a long time. And
even now not everybody clearly appreciates that the abundance of
random events around us conceal definite (\redem{probabilistic})
patterns.

These ideas prompted me to write this book. I want to help
the reader discover for himself the probabilistic nature of the world
around us, to introduce random phenomena and processes, and to show
that it is possible to orient oneself in this random world and to
operate effectively within it.  

This book begins with a talk between myself and an imaginary reader
about the role of chance, and ends with another talk about the
relationship between randomness and symmetry. The text is divided into
two major parts. The first is on the concept of probability and
considers the various applications of probability in practice, namely,
making decisions in complicated situations, organizing queues,
participating in games, optimizing the control of various processes,
and doing random searches. The basic notions of cybernetics,
information theory, and such comparatively new fields as operations
research and the theory of games are given. The aim of the first part
is to convince the reader that the random world begins directly in his
own living room because, in fact, all modern life is based on
probabilistic methods. The second part shows how fundamental chance is
in Nature using the probabilistic laws of modern physics and biology
as examples. Elements of quantum mechanics are also involved, and this
allows me to demonstrate how probabilistic laws are basic to
microscopic phenomena. The idea was that by passing from the first
part of the book to the second one, the reader would see that
probability is not only around us but is at the basis of everything.

In conclusion I would like to express my gratitude to everyone who
helped me when writing this book. I.I. Gurevich, Corresponding
Member of the USSR Academy of Sciences, gave me the idea of writing this text and gave me a number of other provoking ideas concerning the material and structure of the book. B.V. Gnedenko, Member of the USSR Academy of Sciences, G.Ya. Myakishev, D.Sc. (Philosophy), and O.F. Kabardin. Cand. Sc. (Physics and Mathematics) read the manuscript thoroughly and made valuable remarks. V.A. Ezhiv and A.N. Tarasova rendered me constant advice and unstinting support the whole time I was preparing the text. 

\cleardoublepage


%%% Local Variables:
%%% mode: latex
%%% TeX-engine: xetex
%%% TeX-master: "tarasov-twibop"
%%% End:
